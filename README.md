This repository is for the data collection for the project Mining specialized knowledge from online communities at Tilburg University

1. The annotators provide JSON-files given a pre-defined schema.
2. Run prepare.py to create 2 dictionaries from the JSON-s which can be used to calculate the statistics.

Annotator Agreement
...................

3. Run statistics.py to get Precision, Recall and F-score on question_ids and answer_ids.
4. Run method_compare.py to get Precision, Recall and F-score on the methods.

Method signature resolution
...........................

5. Run merger.py to merge the results of the annotators
6. Run indexer.py to get the data for the resolution
7. Run resolver.py to resolve the method signatures
