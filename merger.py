import pickle
from pprint import pprint
import json

#The input to this script is a dictionary in the form of {answer_id: [qustion_id, methods]} for the 2 annotators


class parser():
    #Creates a list of dictionaries from the original dictionary
	def dict_parser(self, dictionary):
        new_dict = []
        big_dict = {}
        for i in dictionary:
            new_dict.append({dictionary[i][0]: dictionary[i][1:]})

        return new_dict
		
	#Merges dictinaries from a list of dictionaries
    def merge(self, new_dict):
        big_dict = {}
        for i in new_dict:
            k = i.keys()[0]
            v = i.values()[0]
            if k in big_dict:
                big_dict[k].extend(v)
            else:
                big_dict[k] = v
        return big_dict
	
	#Calls the 2 functions together
    def parse(self, dictonary):
        new_dict = self.dict_parser(dictonary)
        big_dict = self.merge(new_dict)
        return big_dict


#Initializing parser and reading in the dictionaries of the annotators
parser = parser()
akos_in = open('akos_methods.txt')
akos = pickle.load(akos_in)
anto_in = open('anto_methods.txt')
anto = pickle.load(anto_in)

#Creating a list of dictionaries from the original dictionaries and merging them separately
new_akos = parser.parse(akos)
new_anto = parser.parse(anto)
new_new_akos = []
new_new_anto = []

#Creating a list of dictionaries
for i in new_akos:
    new_new_akos.append({i : new_akos[i]})

for i in new_anto:
    new_new_anto.append({i: new_anto[i]})

#Merging the two annotators results
whole_dict = new_new_akos+new_new_anto
merged = parser.merge(whole_dict)
for i in merged:
    merged[i] = list(set(merged[i]))

#Saving the merged dictionary out to a new JSON
json_out = open('merged.json', 'w')
json.dump(merged, json_out, indent=5)