import json
from pprint import pprint
import pickle

#There remain a lot of methods unresolved. One portion of them is because of typo-s 
#Another portion is because there are not part of the standard library 
#And yet another portion that needs to be dealt with

#Reading in the merged file and the method signatures
methods_in = open('merged.json').read()
method_dict = open('methods_dict')
index = pickle.load(method_dict)
questions = json.loads(methods_in)


#Looping over the question ids
#New dictionaries parent is the question id
#Loop over the methods under the id, the method is the daughter in the new_dict
#Take the index and search for an entry which Name and fulltype is equal to the class and name of the method
#If there is one append the indexes content to the new dictionary

new_dic = {}
for question in questions:
    new_dic[question] = {}
    for method in (questions[question]):
            methoda = method.split('.')
            new_dic[question][method] = {}
            if len(methoda) == 2:
                name = methoda[1]
                typ = methoda[0]
                for i in index:
                    if i.split('_')[1] == name and index[i]['fulltype'].split('.')[-1] == typ:
                        new_dic[question][method] = index[i]

pprint(new_dic)