import pickle

#This script reads in the method-signatures extracted with the xmlextr.py from codeine: Signature_original.txt
#It creates a dictionary where the keys are the names of the methods
#And the values are the data in the extracted method-signatures
#It is saved out to methods_dict

index = open('Signature_original.txt').readlines()
counter = 0
methods_dict = {}
for i in index:
    signature = i.split()
    counter += 1
    methods_dict[str(counter)+'_'+signature[0]] = {}
    for j in signature[3:]:
        element = j.split(':')
        methods_dict[str(counter)+'_'+signature[0]][element[0]] = element[1]


methods_dict_out = open('methods_dict','w')
pickle.dump(methods_dict, methods_dict_out)




