import pickle
import pprint
from statistics import statistics

#This function discards the class from the method
class preprocesser:
    def preprocess(self, methods):
        clean = {}
        for i in methods:
            clean[i] = []
            for j in methods[i]:
                a = j.split('.')
                if len(a) > 1:
                    method = a[1].lower().split('(')[0]
                else:
                    method = a[0].lower().split('(')[0]
                clean[i].append(method)
        return clean




class method_statistics:
    #This counts the number of times we get the whole list of methods right per answer
    #Here you only score if the whole list of methods is exactly the same
    #You basically get 0 if even one element in the list is incorrect
    def micro(self, annotator, target):
        annotator_full = []
        target_full = []
        for answer_id in annotator.keys():
            if len(annotator[answer_id]) != 0 and len(target[answer_id]) != 0:
                annotator_full.append((annotator[answer_id], answer_id))
                target_full.append((target[answer_id], answer_id))
        precision = s.precision(annotator_full, target_full)
        recall = s.recall(annotator_full, target_full)
        s.f_score(annotator_full, target_full)
        print "Precision:", precision
        print 'Recall:', recall

    #This counts precision and recall by answer_id
    #It counts the number of methods the annotator got right against the target for every answer_id
    def macro(self, annotator, target):
        precision = 0.0
        recall = 0.0
        counter = 0.0
        f_score = 0.0
        for answer_id in annotator.keys():
            if len(annotator[answer_id]) != 0 and len(target[answer_id]) != 0:
                prec = s.precision(annotator[answer_id], target[answer_id])
                rec = s.recall(annotator[answer_id], target[answer_id])
                if prec == 0 or rec == 0:
                    f = 0
                else:
                    f = ((prec*rec)/(prec+rec))*2
                precision += prec
                recall += rec
                f_score += f
                counter += 1
        precision = precision/counter
        recall = recall/counter
        f_score = f_score/counter
        print "Precision:" ,precision
        print "Recall",recall
        print "F-score", f_score

    #This function checks the overlap per answer id and divides it by the total number of methods
    def micromacro(self, annotator, target):
        num_correct = 0
        annotator_full = []
        target_full = []
        for answer_id in annotator.keys():
            if len(annotator[answer_id]) != 0 and len(target[answer_id]) != 0:
                annotator_full += annotator[answer_id]
                target_full += target[answer_id]
        for answer_id in annotator.keys():
            if len(annotator[answer_id]) != 0 and len(target[answer_id]) != 0:
                num_correct += len([x for x in annotator[answer_id] if x in target[answer_id]])
        precision = num_correct/float(len(annotator_full))
        recall = num_correct/float(len(target_full))
        f_score = ((precision*recall)/(precision+recall))*2
        print "Precision:", precision
        print "Recall", recall
        print 'F-score', f_score




akos_in = open('akos_methods.txt')
akos = pickle.load(akos_in)
anto_in = open('anto_methods.txt')
anto = pickle.load(anto_in)
p = preprocesser()
s = statistics()
m = method_statistics()
akos_dict = p.preprocess(akos)
anto_dict = p.preprocess(anto)
print
print "Sum of overlap per answer_id divided by the number of guessed/target methods"
print "-------------------"
print
print "Ak against An"
print "..................."
akos_vs_anto = m.micromacro(akos_dict, anto_dict)
print
print "An against Ak"
print ".............."
anto_vs_akos = m.micromacro(anto_dict, akos_dict)
print
#akos_vs_anto = micro(akos_dict, anto_dict)
print
#anto_vs_akos = micro(anto_dict, akos_dict)
#micromacro(akos_dict, anto_dict)
#micromacro(anto_dict, akos_dict)
