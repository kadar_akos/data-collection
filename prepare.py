import json
import pprint
import pickle

#Running the whole code creates 2 dictionaries for the 2 annotators in the form of {answer_id: [method1, method2]}
#It removes all answers that one of the annotators does not have for the later comparison on the methods

#This class is used to read in the JSON's and retrieve the answer and question ids from them
#By calling the parse method it returns a cleaned version of the dictionary, a list of answer ids and a list of question ids
class parser:
    def read(self, name):
        fil = open(name)
        text = fil.read()
        answeres = json.loads(text)['answeres']
        return answeres

    def getQuestion_ids(self, json):
        question_ids = []
        for i in json:
            question_ids.append(i['question_id'])
        akos_questions = list(set(question_ids))
        return akos_questions

    def getAnswer_ids(self, json):
        answer_ids = []
        for i in json:
            answer_ids.append(i['answer_id'])
        answer_ids = list(set(answer_ids))
        return answer_ids

    def clean(self, json):
        for i in json:
            if i['methods'] == None:
                json.remove(i)
        return json

    def parse(self, name):
        answeres = self.read(name)
        question_ids = self.getQuestion_ids(answeres)
        answer_ids = self.getAnswer_ids(answeres)
        answeres = self.clean(answeres)
        return answeres, question_ids, answer_ids

#This class is used to remove entries from the dictionaries based on the ideas that the other dictionary do not have
class entry_remover:
    def getIds(self, list1, list2):
        ids = [x for x in list1 if x in list2]
        ids.sort()
        return ids

    def remove(self, json, list1, list2, key):
        ids = self.getIds(list1, list2)
        new_list = []
        for i in json:
            if i[key] in ids:
                new_list.append(i)
        return new_list

#This class is used to write out the final version of the dictionary for the later comparison of the methods
class new_data:
    def getNew(self, json, methods_out):
        final_methods = {}
        for i in json:
            answer_id = i['answer_id']
            methods = []
            for j in i['methods']:
                if j != 0:
                    methods.append(j['method_name'])
            final_methods[answer_id] = methods
        #out = open(methods_out, 'w')
        print final_methods
        #pickle.dump(final_methods, out)


parser = parser()
remover = entry_remover()
final = new_data()

akos, akos_questions, akos_answeres = parser.parse('answer(1).json')
anto, anto_questions, anto_answeres = parser.parse('answer(2).json')
new_akos = remover.remove(akos, akos_questions, anto_questions, 'question_id')
new_anto = remover.remove(anto, akos_questions, anto_questions, 'question_id')
akos_answeres = parser.getAnswer_ids(new_akos)
anto_answeres = parser.getAnswer_ids(new_anto)
new_akos = remover.remove(new_akos, akos_answeres, anto_answeres, 'answer_id')
new_anto = remover.remove(new_anto, akos_answeres, anto_answeres, 'answer_id')

final.getNew(new_akos,'akos_methods.txt')
#final.getNew(new_anto, 'anto_methods.txt')

print len(new_anto), len(new_akos)
print sorted(parser.getQuestion_ids(new_akos))
print sorted(parser.getQuestion_ids(new_anto))
