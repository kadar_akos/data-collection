from new import parser

class statistics:
    def precision(self, annotator, target):
        precision = len([x for x in annotator if x in target])/float(len(annotator))
        return precision

    def recall(self, annotator, target):
        recall = len([x for x in annotator if x in target])/float(len(target))
        return recall

    def f_score(self, annotator, target):
        precision = self.precision(annotator, target)
        recall = self.recall(annotator, target)
        f_score = ((precision*recall)/(precision+recall))*2
        return f_score


akos, akos_questions, akos_answeres = parser.parse('answer(1).json')
anto, anto_questions, anto_answeres = parser.parse('answer(2).json')

s = statistics()
print "Answers"
print "----------------------"
print
print "Akos against Antoaneta"
print "......................"
precision = s.precision(akos_questions, anto_questions)
print "Precision:", round(precision, 3)
recall = s.recall(akos_questions, anto_questions)
print "Recall:", round(recall, 3)
s.f_score(akos_questions, anto_questions)
print
print "Antoaneta against Akos"
print "......................"
precision = s.precision(anto_questions, akos_questions)
print "Precision:", round(precision, 3)
recall = s.recall(anto_questions, akos_questions)
print "Recall:", round(recall, 3)
s.f_score(anto_questions, akos_questions)
